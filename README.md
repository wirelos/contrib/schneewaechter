[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/wirelos/contrib/schneewaechter)

# Schnee-Wächter
IoT system to collect, analyse and monitor sensor data.  
It is composed of loosely coupled and interchangable components for edge and cloud computing.  
Edge devices and cloud services need to be deployed manually at this stage. Nevertheless, the whole system can be [integrated](https://www.brudi.com/blog/iot-projekte) easily into a comprehensive IoT platform including fleet management and continuous delivery.  

> **Note**  
> This repository contains source code required to run the IoT system according to defined [architecture](./assets/architecture-mvp.png). It does not contain any infrastructure management or continuous integration pipelines.  
> [Operating](https://www.brudi.com/products/ops) and running the corresponding infrastructure for this system requires a capable [IoT platform](https://www.brudi.com/products/iot) for infrastructure und resource provisioning, as well as [container orchestration](https://www.brudi.com/de/blog/microservices-mit-brudi-platform) on edge and cloud systems.  

## Architecture
The following diagram illustrates the implemented architecture.  
In this configuration, it shows the minimal required stack and does not include scalability options and device/fleet management.  

![architecture-mvp](./assets/architecture-mvp.png)

## Project Layout
```
.
├── assets
├── cloud
│   └── monitoring
└── edge
    ├── gateway
    │   ├── boot
    │   │   └── system-connections
    └── sensorbox
        └── firmware
```

## Prerequisites

Local:
- make
- docker

Edge:
- RaspberryPI 3 B/B+
- USB SSD/HDD
- ESP32 TTGO T18

Cloud:
- Kubernetes > 1.14

## Edge
The edge application can be accessed by connecting to the gateway's access point:  

![qr-code](./assets/gateway-qr-code.jpg)

### SensorBox
Sends sensor measurements to a message queue on the locale gateway's message queue over wifi.  
  
Requires a gateway with:
- DHCP server
- MQTT broker

For details about its configuration, take a look at [sensorbox.yaml](./edge/sensorbox/firmware/sensorbox.concat.yaml).  

### Gateway
Collects and stores data from all sensorboxes and forwards aggregated data to the cloud.  
Live data can be inspected and various maintenance tasks (e.g. reprogramming the SensorBox) performed directly on the gateway.  

![dashboard](./assets/dashboard-edge.png)

## Cloud
The aggregated edge data is also sent to a timeseries database in the cloud 
for longterm storage and further processing, analysis and monitoring.

## Monitoring
Dashboards for basic monitoring can be deployed to edge and cloud instances of Grafana.  

![dashboard](./assets/dashboard-cloud.png)

## Open Source
The edge stack is partially based on [Balena Node TIG Stack](https://github.com/janvda/balena-node-red-mqtt-nginx-TIG-stack).