FROM gitpod/workspace-full-vnc

USER root
RUN apt-get update && apt-get install -y \
        # dev
        proxytunnel \
        kpartx \
        sqlite3 \
        # ui
        libasound2-dev \
        libgtk-3-dev \
        libnss3-dev \
    && rm -rf /var/lib/apt/lists/*

ENV BALENA_CLI_VERSION v12.26.1
ENV BALENA_RELEASE https://github.com/balena-io/balena-cli/releases/download/$BALENA_CLI_VERSION/balena-cli-$BALENA_CLI_VERSION-linux-x64-standalone.zip

RUN curl -sSLo balena-cli-$BALENA_CLI_VERSION.zip $BALENA_RELEASE && \
    unzip balena-cli-$BALENA_CLI_VERSION.zip && \
    mv balena-cli /opt && \
    rm -rf balena-cli-$BALENA_CLI_VERSION.zip
RUN pip3 install esphome esptool

USER gitpod
ENV PATH /opt/balena-cli:$PATH