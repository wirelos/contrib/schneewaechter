.PHONY: drunit dbuild compose-build

CTX_PATH ?= $(shell pwd)
WORKDIR ?= $(shell pwd)
RECIPE ?= Dockerfile

drunit:
	docker run -it --rm --tty --privileged --network=host \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v ${WORKDIR}:/usr/app \
		-u root \
		-w /usr/app \
		${IMAGE} /bin/bash -c "${CMD}"

dbuild:
	@docker build \
		--file ${RECIPE} \
		--tag ${TAG} \
		${CTX_PATH}