#!/bin/bash

mkdir -p /mnt/edge-disk/
mount -t exfat-fuse -o rw -L edge-disk /mnt/edge-disk
mkdir -p /mnt/edge-disk/influxdb
