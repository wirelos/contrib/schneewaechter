# SensorBox
Firmware for wireless sensor logging on ESP32.  


## Usage
Run `make help`:  
```

 SensorBox Firmware Workspace
 ----------------------------

 Usage: 
 make <target>

 check          Validate YAML configuration
 wizard         Create new configuration
 build          Generate and build the firmware
 run            Build, flash and monitor attached `$DEVICE` (Default: /dev/ttyUSB0)
 dashboard      Visual tool to manage the firmware configuration
 dev            Runs dashboard with an attached `$DEVICE` (Default: /dev/ttyUSB0)
 clean          Cleanup build folders
```