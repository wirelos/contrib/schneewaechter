// Auto generated code by esphome
// ========== AUTO GENERATED INCLUDE BLOCK BEGIN ===========
#include "esphome.h"
using namespace esphome;
logger::Logger *logger_logger;
web_server_base::WebServerBase *web_server_base_webserverbase;
captive_portal::CaptivePortal *captive_portal_captiveportal;
wifi::WiFiComponent *wifi_wificomponent;
ota::OTAComponent *ota_otacomponent;
mqtt::MQTTClientComponent *mqtt_mqttclientcomponent;
using namespace sensor;
using namespace mqtt;
mqtt::MQTTMessageTrigger *mqtt_mqttmessagetrigger;
web_server::WebServer *web_server_webserver;
Automation<std::string> *automation;
using namespace json;
adc::ADCSensor *dms;
adc::ADCSensor *vref;
adc::ADCSensor *humidity;
adc::ADCSensor *temperature;
wifi_signal::WiFiSignalSensor *wifi_signal_wifisignalsensor;
deep_sleep::DeepSleepComponent *deep_sleep_1;
mqtt::MQTTSensorComponent *mqtt_mqttsensorcomponent;
mqtt::MQTTSensorComponent *mqtt_mqttsensorcomponent_2;
mqtt::MQTTSensorComponent *mqtt_mqttsensorcomponent_3;
mqtt::MQTTSensorComponent *mqtt_mqttsensorcomponent_4;
mqtt::MQTTSensorComponent *mqtt_mqttsensorcomponent_5;
deep_sleep::PreventDeepSleepAction<std::string> *deep_sleep_preventdeepsleepaction;
mqtt::MQTTMessageTrigger *mqtt_mqttmessagetrigger_2;
Automation<std::string> *automation_2;
deep_sleep::EnterDeepSleepAction<std::string> *deep_sleep_enterdeepsleepaction;
// ========== AUTO GENERATED INCLUDE BLOCK END ==========="

void setup() {
  // ===== DO NOT EDIT ANYTHING BELOW THIS LINE =====
  // ========== AUTO GENERATED CODE BEGIN ===========
  // async_tcp:
  // esphome:
  //   name: mockbox
  //   platform: ESP32
  //   board: heltec_wifi_lora_32
  //   arduino_version: espressif32@1.12.4
  //   build_path: mockbox
  //   platformio_options: {}
  //   includes: []
  //   libraries: []
  App.pre_setup("mockbox", __DATE__ ", " __TIME__);
  // logger:
  //   baud_rate: 115200
  //   level: DEBUG
  //   id: logger_logger
  //   tx_buffer_size: 512
  //   hardware_uart: UART0
  //   logs: {}
  logger_logger = new logger::Logger(115200, 512, logger::UART_SELECTION_UART0);
  logger_logger->pre_setup();
  App.register_component(logger_logger);
  // web_server_base:
  //   id: web_server_base_webserverbase
  web_server_base_webserverbase = new web_server_base::WebServerBase();
  App.register_component(web_server_base_webserverbase);
  // captive_portal:
  //   id: captive_portal_captiveportal
  //   web_server_base_id: web_server_base_webserverbase
  captive_portal_captiveportal = new captive_portal::CaptivePortal(web_server_base_webserverbase);
  App.register_component(captive_portal_captiveportal);
  // wifi:
  //   fast_connect: true
  //   ap:
  //     ssid: MockBox
  //     password: th3r31sn0sp00n
  //     id: wifi_wifiap
  //     ap_timeout: 1min
  //   id: wifi_wificomponent
  //   domain: .local
  //   reboot_timeout: 15min
  //   power_save_mode: LIGHT
  //   networks:
  //   - ssid: SchneeWaechter
  //     password: th3r31sn0sp00n
  //     id: wifi_wifiap_2
  //     priority: 0.0
  //   use_address: mockbox.local
  wifi_wificomponent = new wifi::WiFiComponent();
  wifi_wificomponent->set_use_address("mockbox.local");
  wifi::WiFiAP wifi_wifiap_2 = wifi::WiFiAP();
  wifi_wifiap_2.set_ssid("SchneeWaechter");
  wifi_wifiap_2.set_password("th3r31sn0sp00n");
  wifi_wifiap_2.set_priority(0.0f);
  wifi_wificomponent->add_sta(wifi_wifiap_2);
  wifi::WiFiAP wifi_wifiap = wifi::WiFiAP();
  wifi_wifiap.set_ssid("MockBox");
  wifi_wifiap.set_password("th3r31sn0sp00n");
  wifi_wificomponent->set_ap(wifi_wifiap);
  wifi_wificomponent->set_ap_timeout(60000);
  wifi_wificomponent->set_reboot_timeout(900000);
  wifi_wificomponent->set_power_save_mode(wifi::WIFI_POWER_SAVE_LIGHT);
  wifi_wificomponent->set_fast_connect(true);
  App.register_component(wifi_wificomponent);
  // ota:
  //   safe_mode: true
  //   password: th3r31sn0sp00n
  //   id: ota_otacomponent
  //   port: 3232
  ota_otacomponent = new ota::OTAComponent();
  ota_otacomponent->set_port(3232);
  ota_otacomponent->set_auth_password("th3r31sn0sp00n");
  App.register_component(ota_otacomponent);
  ota_otacomponent->start_safe_mode();
  // mqtt:
  //   broker: 10.42.0.1
  //   on_message:
  //   - topic: mockbox/keep_alive
  //     payload: 'ON'
  //     then:
  //     - deep_sleep.prevent:
  //         id: deep_sleep_1
  //       type_id: deep_sleep_preventdeepsleepaction
  //     automation_id: automation
  //     trigger_id: mqtt_mqttmessagetrigger
  //     qos: 0
  //   - topic: mockbox/sleep
  //     payload: 'ON'
  //     then:
  //     - deep_sleep.enter:
  //         id: deep_sleep_1
  //       type_id: deep_sleep_enterdeepsleepaction
  //     automation_id: automation_2
  //     trigger_id: mqtt_mqttmessagetrigger_2
  //     qos: 0
  //   id: mqtt_mqttclientcomponent
  //   port: 1883
  //   username: ''
  //   password: ''
  //   discovery: true
  //   discovery_retain: true
  //   discovery_prefix: homeassistant
  //   topic_prefix: mockbox
  //   keepalive: 15s
  //   reboot_timeout: 15min
  //   birth_message:
  //     topic: mockbox/status
  //     payload: online
  //     qos: 0
  //     retain: true
  //   will_message:
  //     topic: mockbox/status
  //     payload: offline
  //     qos: 0
  //     retain: true
  //   shutdown_message:
  //     topic: mockbox/status
  //     payload: offline
  //     qos: 0
  //     retain: true
  //   log_topic:
  //     topic: mockbox/debug
  //     qos: 0
  //     retain: true
  mqtt_mqttclientcomponent = new mqtt::MQTTClientComponent();
  App.register_component(mqtt_mqttclientcomponent);
  // web_server:
  //   port: 80
  //   auth:
  //     username: user
  //     password: th3r31sn0sp00n
  //   id: web_server_webserver
  //   css_url: https:esphome.io/_static/webserver-v1.min.css
  //   js_url: https:esphome.io/_static/webserver-v1.min.js
  //   web_server_base_id: web_server_base_webserverbase
  // sensor:
  mqtt_mqttclientcomponent->set_broker_address("10.42.0.1");
  mqtt_mqttclientcomponent->set_broker_port(1883);
  mqtt_mqttclientcomponent->set_username("");
  mqtt_mqttclientcomponent->set_password("");
  mqtt_mqttclientcomponent->set_discovery_info("homeassistant", true);
  mqtt_mqttclientcomponent->set_topic_prefix("mockbox");
  mqtt_mqttclientcomponent->set_birth_message(mqtt::MQTTMessage{
      .topic = "mockbox/status",
      .payload = "online",
      .qos = 0,
      .retain = true,
  });
  mqtt_mqttclientcomponent->set_last_will(mqtt::MQTTMessage{
      .topic = "mockbox/status",
      .payload = "offline",
      .qos = 0,
      .retain = true,
  });
  mqtt_mqttclientcomponent->set_shutdown_message(mqtt::MQTTMessage{
      .topic = "mockbox/status",
      .payload = "offline",
      .qos = 0,
      .retain = true,
  });
  mqtt_mqttclientcomponent->set_log_message_template(mqtt::MQTTMessage{
      .topic = "mockbox/debug",
      .payload = "",
      .qos = 0,
      .retain = true,
  });
  mqtt_mqttclientcomponent->set_keep_alive(15);
  mqtt_mqttclientcomponent->set_reboot_timeout(900000);
  mqtt_mqttmessagetrigger = new mqtt::MQTTMessageTrigger("mockbox/keep_alive");
  mqtt_mqttmessagetrigger->set_qos(0);
  mqtt_mqttmessagetrigger->set_payload("ON");
  App.register_component(mqtt_mqttmessagetrigger);
  web_server_webserver = new web_server::WebServer(web_server_base_webserverbase);
  App.register_component(web_server_webserver);
  automation = new Automation<std::string>(mqtt_mqttmessagetrigger);
  web_server_base_webserverbase->set_port(80);
  web_server_webserver->set_css_url("https://esphome.io/_static/webserver-v1.min.css");
  web_server_webserver->set_js_url("https://esphome.io/_static/webserver-v1.min.js");
  web_server_webserver->set_username("user");
  web_server_webserver->set_password("th3r31sn0sp00n");
  // json:
  // substitutions:
  //   device_name: mockbox
  //   device_board: heltec_wifi_lora_32
  //   device_platform: ESP32
  //   gw_ssid: SchneeWaechter
  //   gw_pwd: th3r31sn0sp00n
  //   ap_ssid: MockBox
  //   ap_pwd: th3r31sn0sp00n
  //   log_level: DEBUG
  //   ota_pwd: th3r31sn0sp00n
  //   ws_user: user
  //   ws_pwd: th3r31sn0sp00n
  //   broker_ip: 10.42.0.1
  //   pin_dms: '36'
  //   pin_vref: '37'
  //   pin_humidity: '38'
  //   pin_thermistor: '39'
  //   adc_update_interval: 1000ms
  //   adc_accuracy_decimals: '8'
  //   wifi_check_interval: 60s
  //   deep_sleep_event: deep_sleep_1
  //   deep_sleep_duration: 1min
  //   deep_sleep_run_duration: 5min
  // sensor.adc:
  //   platform: adc
  //   id: dms
  //   name: dms
  //   pin: 36
  //   update_interval: 1000ms
  //   attenuation: 11db
  //   accuracy_decimals: 8
  //   mqtt_id: mqtt_mqttsensorcomponent
  //   force_update: false
  //   unit_of_measurement: V
  //   icon: mdi:flash
  dms = new adc::ADCSensor();
  dms->set_update_interval(1000);
  App.register_component(dms);
  // sensor.adc:
  //   platform: adc
  //   id: vref
  //   name: vref
  //   pin: 37
  //   update_interval: 1000ms
  //   attenuation: 11db
  //   accuracy_decimals: 8
  //   mqtt_id: mqtt_mqttsensorcomponent_2
  //   force_update: false
  //   unit_of_measurement: V
  //   icon: mdi:flash
  vref = new adc::ADCSensor();
  vref->set_update_interval(1000);
  App.register_component(vref);
  // sensor.adc:
  //   platform: adc
  //   id: humidity
  //   name: Humidity
  //   pin: 38
  //   update_interval: 1000ms
  //   attenuation: 11db
  //   accuracy_decimals: 8
  //   mqtt_id: mqtt_mqttsensorcomponent_3
  //   force_update: false
  //   unit_of_measurement: V
  //   icon: mdi:flash
  humidity = new adc::ADCSensor();
  humidity->set_update_interval(1000);
  App.register_component(humidity);
  // sensor.adc:
  //   platform: adc
  //   name: Temperature
  //   id: temperature
  //   pin: 39
  //   attenuation: 11db
  //   update_interval: 1000ms
  //   mqtt_id: mqtt_mqttsensorcomponent_4
  //   force_update: false
  //   unit_of_measurement: V
  //   icon: mdi:flash
  //   accuracy_decimals: 2
  temperature = new adc::ADCSensor();
  temperature->set_update_interval(1000);
  App.register_component(temperature);
  // sensor.wifi_signal:
  //   platform: wifi_signal
  //   name: WiFi Signal Strength
  //   update_interval: 60s
  //   mqtt_id: mqtt_mqttsensorcomponent_5
  //   force_update: false
  //   unit_of_measurement: dB
  //   icon: mdi:wifi
  //   accuracy_decimals: 0
  //   id: wifi_signal_wifisignalsensor
  wifi_signal_wifisignalsensor = new wifi_signal::WiFiSignalSensor();
  wifi_signal_wifisignalsensor->set_update_interval(60000);
  App.register_component(wifi_signal_wifisignalsensor);
  // deep_sleep:
  //   id: deep_sleep_1
  //   run_duration: 5min
  //   sleep_duration: 1min
  deep_sleep_1 = new deep_sleep::DeepSleepComponent();
  App.register_component(deep_sleep_1);
  App.register_sensor(dms);
  dms->set_name("dms");
  dms->set_unit_of_measurement("V");
  dms->set_icon("mdi:flash");
  dms->set_accuracy_decimals(8);
  dms->set_force_update(false);
  mqtt_mqttsensorcomponent = new mqtt::MQTTSensorComponent(dms);
  App.register_component(mqtt_mqttsensorcomponent);
  App.register_sensor(vref);
  vref->set_name("vref");
  vref->set_unit_of_measurement("V");
  vref->set_icon("mdi:flash");
  vref->set_accuracy_decimals(8);
  vref->set_force_update(false);
  mqtt_mqttsensorcomponent_2 = new mqtt::MQTTSensorComponent(vref);
  App.register_component(mqtt_mqttsensorcomponent_2);
  App.register_sensor(humidity);
  humidity->set_name("Humidity");
  humidity->set_unit_of_measurement("V");
  humidity->set_icon("mdi:flash");
  humidity->set_accuracy_decimals(8);
  humidity->set_force_update(false);
  mqtt_mqttsensorcomponent_3 = new mqtt::MQTTSensorComponent(humidity);
  App.register_component(mqtt_mqttsensorcomponent_3);
  App.register_sensor(temperature);
  temperature->set_name("Temperature");
  temperature->set_unit_of_measurement("V");
  temperature->set_icon("mdi:flash");
  temperature->set_accuracy_decimals(2);
  temperature->set_force_update(false);
  mqtt_mqttsensorcomponent_4 = new mqtt::MQTTSensorComponent(temperature);
  App.register_component(mqtt_mqttsensorcomponent_4);
  App.register_sensor(wifi_signal_wifisignalsensor);
  wifi_signal_wifisignalsensor->set_name("WiFi Signal Strength");
  wifi_signal_wifisignalsensor->set_unit_of_measurement("dB");
  wifi_signal_wifisignalsensor->set_icon("mdi:wifi");
  wifi_signal_wifisignalsensor->set_accuracy_decimals(0);
  wifi_signal_wifisignalsensor->set_force_update(false);
  mqtt_mqttsensorcomponent_5 = new mqtt::MQTTSensorComponent(wifi_signal_wifisignalsensor);
  App.register_component(mqtt_mqttsensorcomponent_5);
  deep_sleep_1->set_sleep_duration(60000);
  deep_sleep_1->set_run_duration(300000);
  deep_sleep_preventdeepsleepaction = new deep_sleep::PreventDeepSleepAction<std::string>(deep_sleep_1);
  dms->set_pin(36);
  dms->set_attenuation(ADC_11db);
  vref->set_pin(37);
  vref->set_attenuation(ADC_11db);
  humidity->set_pin(38);
  humidity->set_attenuation(ADC_11db);
  temperature->set_pin(39);
  temperature->set_attenuation(ADC_11db);
  automation->add_actions({deep_sleep_preventdeepsleepaction});
  mqtt_mqttmessagetrigger_2 = new mqtt::MQTTMessageTrigger("mockbox/sleep");
  mqtt_mqttmessagetrigger_2->set_qos(0);
  mqtt_mqttmessagetrigger_2->set_payload("ON");
  App.register_component(mqtt_mqttmessagetrigger_2);
  automation_2 = new Automation<std::string>(mqtt_mqttmessagetrigger_2);
  deep_sleep_enterdeepsleepaction = new deep_sleep::EnterDeepSleepAction<std::string>(deep_sleep_1);
  automation_2->add_actions({deep_sleep_enterdeepsleepaction});
  // =========== AUTO GENERATED CODE END ============
  // ========= YOU CAN EDIT AFTER THIS LINE =========
  App.setup();
}

void loop() {
  App.loop();
}
