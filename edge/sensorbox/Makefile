##
## SensorBox
## ----------------------------
##
WORKSPACE		?=	$(shell pwd)/firmware
RUNNER			?=	esphome/esphome
DEVICE			?=	/dev/ttyUSB0
DEVICE_NAME		?=	sensorbox
SPECS 			?=	${DEVICE_NAME}.yaml
ITSO			=	docker run --rm -it -v ${WORKSPACE}:/config

.PHONY: 
	init \
	wizard \
	build
	run \
	dashboard \
	dev \
	clean

## Usage: 
## make <target>
##
help : Makefile
	@sed -n 's/^##//p' $<

## check		Validate YAML configuration
check:
	@$(ITSO) ${RUNNER} ${SPECS} config 

## wizard		Create new configuration
wizard:
	@$(ITSO) ${RUNNER} ${SPECS} wizard

## build		Generate and build the firmware
build: clean
	@$(ITSO) ${RUNNER} ${SPECS} compile

## run		Build, flash and monitor attached `$DEVICE` (Default: /dev/ttyUSB0)
run:
	@$(ITSO) --device=${DEVICE} ${RUNNER} ${SPECS} run

## dashboard	Visual tool to manage the firmware configuration
dashboard:
	@$(ITSO) -p 6052:6052 ${RUNNER} /config dashboard

## dev		Runs dashboard with an attached `$DEVICE` (Default: /dev/ttyUSB0)
dev:
	@$(ITSO) -p 6052:6052 ${RUNNER} --device=${DEVICE} /config dashboard

## clean		Cleanup build folders
clean:
	@$(ITSO) ${RUNNER} ${SPECS} clean

##
## ---
##